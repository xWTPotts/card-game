//
//  Dice.swift
//  FCC Test
//
//  Created by Taylor Potts on 2021-07-26.
//

import Foundation

class Dice {
    var value: Int
    
    init() {
        value = Int.random(in: 1...6)
    }
}
