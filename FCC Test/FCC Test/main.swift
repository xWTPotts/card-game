//
//  main.swift
//  FCC Test
//
//  Created by Taylor Potts on 2021-07-26.
//

//Dice Game

import Foundation


var totalScores: [Int] = []
var numberOfGames = 10000
var numberOfDice = 5


// MARK: For user input of Number of Games and Dice
/* print("Enter number of Games: ")
if let numGames = readLine() {
    if let int = Int(numGames) {
        numberOfGames = int
    }
}

print("Enter number of Dice: ")
if let numOfDice = readLine() {
    if let int = Int(numOfDice) {
        numberOfDice = int
    }
} */

repeat {
    let diceGame = DiceGame()
    diceGame.setupGame(numberOfDice: numberOfDice)
    diceGame.playGame()
    totalScores.append(diceGame.totalScore)
    numberOfGames -= 1
    
} while numberOfGames > 0

findDuplicateValues(totalScores: totalScores)

func findDuplicateValues(totalScores: [Int]) {
    
    let tupleForTotalScores = totalScores.map { ($0, 1)}
    let scoreFrequency = Dictionary(tupleForTotalScores, uniquingKeysWith: +).sorted(by: (<))
    for score in scoreFrequency {
        printScore(score: score.key, count: score.value)
    }
}

func printScore(score: Int, count: Int) {
    print("Total \(score) occured \(count) times")
}



