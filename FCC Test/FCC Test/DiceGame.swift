//
//  DiceGame.swift
//  FCC Test
//
//  Created by Taylor Potts on 2021-07-26.
//

import Foundation

class DiceGame {
    var totalDice: [Dice] = []
    var remainingDice: [Dice] = []
    var totalScore = 0
    
    func setupGame(numberOfDice: Int) {
        totalScore = 0
        for _ in 1...numberOfDice {
            let newDice = Dice()
            totalDice.append(newDice)
            remainingDice = totalDice
        }
    }
    
    func playGame() {
        repeat {
            rollDice(dice: remainingDice)
        } while remainingDice.count > 0
    }
    
    func rollDice(dice: [Dice]) {
        if dice.contains(where: {$0.value == 3 }) {
            // Remove all dice values that equal 3
            remainingDice = dice.filter { $0.value != 3 }
            totalScore += 0
        }
        else {
            // Add the minimum dice value to the total score
            let minimumDiceValue = dice.min{ $0.value < $1.value }?.value ?? 0
            totalScore += minimumDiceValue
            remainingDice = dice.filter { $0.value != minimumDiceValue }
        }
    }
}
